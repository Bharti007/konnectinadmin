import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../auth.service';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent {
 users:any=[];
 onlineUsers:any=[];
   constructor(public auth: AuthService) {
    this.auth.getAppUsers().then(resp => {
      if(resp ){
          this.users = resp;
          this.onlineUsers = this.users.filter(key => key.online === true);
        
      }
    },
      error => {
        this.auth.showToastr('error', "Error", "Something Went wrong")
      }
    );
  }

 


  ngOnInit(): void {
    
  }
}
