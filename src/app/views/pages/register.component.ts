import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { ReactiveFormsModule,FormBuilder, FormGroup } from '@angular/forms';
import { Validator } from '../../validator';
import { AuthService} from '../../auth.service'
@Component({
  templateUrl: 'register.component.html'
})
export class RegisterComponent {
   private emailPasswordForm: FormGroup;
  constructor(public router: Router,public formBuilder: FormBuilder,public auth : AuthService) {
     this.emailPasswordForm = formBuilder.group({
      email: Validator.emailValidator,
      password: Validator.passwordValidator,
      repassword: Validator.passwordValidator,
       fullname: Validator.fullnameValidator
    });
   }

  btnClick() {
    this.router.navigateByUrl('/pages/login');
  };

   register(){
   this.auth.registerUser(this.emailPasswordForm.value["email"], this.emailPasswordForm.value["password"] )
  //  firebase.auth().createUserWithEmailAndPassword(this.emailPasswordForm.value["email"], this.emailPasswordForm.value["password"])
   
    
  }

}
