import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

//Routing
import { EditEventRoutingModule } from './edit-event-routing.module';

import { EditEventComponent } from './edit-event.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
  imports: [
    EditEventRoutingModule,
    BsDropdownModule.forRoot(),
    FormsModule
  ],
  declarations: [
    EditEventComponent
  ]
})
export class EditEventModule { }
