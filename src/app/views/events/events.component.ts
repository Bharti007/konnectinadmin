import { Component } from '@angular/core';
import { AuthService } from '../../auth.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs/observable';
interface PostModel {
  title: string,
  description: string,
  image: string,
  createdOn: string,
  id: string
}
@Component({
  templateUrl: 'events.component.html'
})




export class EventsComponent {
  public primaryModal;
  public dangerModal;
  public action: any = 'Create';
  post: any = {
    title: '',
    description: '',
    image: [],
    createdOn: '',
    id: ''
  }
  posts: any = []
  constructor(public auth: AuthService) {
      this.getPosts();
  }

  dataInPopUp(selecteddata) {
    this.post = {
      title: '',
      description: '',
      image: [],
      createdOn: '',
      id: ''
    }
    if (selecteddata)
      this.post = selecteddata.data
  }

  getPosts() {
    this.auth.getPosts()
      .then(resp => {
        if (resp) {
          this.posts = resp;
        }
      },
      error => {
        this.auth.showToastr('error', "Error", "Something Went wrong")
      }
      );
  }



  updatePost(action, next) {

    if (this.post.title === undefined || this.post.title === ''){
      this.auth.showToastr('error', '', 'Please enter title');
    }
    else if (this.post.description === undefined || this.post.description === ''){
      this.auth.showToastr('error', '', 'Please enter description');
    }

    if (action === 'Create') {
      this.auth.addPost(this.post);

    } else {
      this.auth.updatePost(this.post);

    }
    this.getPosts();
    next;
  }

  deletePost(next) {
    this.auth.deletePost(this.post.id);
    this.getPosts();
    next;
  }



}
